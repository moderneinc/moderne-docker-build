# Use Ubuntu latest jammy
FROM docker:24.0.5

# Set environment variables to avoid interactive prompts during installation
ENV DEBIAN_FRONTEND=noninteractive

# Install necessary packages and dependencies
RUN apk add --no-cache \
    jq \
    zip \
    curl \
    unzip

# Set the default command to run when the container starts.
CMD ["sh"]